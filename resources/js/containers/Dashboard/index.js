import {React, useEffect} from 'react';
import { createBrowserHistory } from 'history';
import {toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {
    PowerSettingsNewTwoTone as PowerOff,
    GroupRounded as TotalEmployees,
    CalendarMonth as TotalLeaves,
    Sick as SickLeave,
    Event as CasualLeaves,
    Settings as ManageRoles,
    AdminPanelSettings as ManagePermissions
} from '@mui/icons-material';
import { MDBDataTable } from 'mdbreact';

export default function Dashboard() {
    let history = createBrowserHistory({forceRefresh:true});
    const userEmail = localStorage.getItem('users');
    const logOut = () => {
        if(confirm('Do you want to log out?')) {
            localStorage.removeItem('users')
            localStorage.removeItem('token')
            localStorage.removeItem('roleName')
            return history.push("/login")
        }
    }

    const data = {
        columns: [
          {
            label: 'Leave Type',
            field: 'leaveType',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Leave Cause',
            field: 'leaveCause',
            sort: 'asc',
            width: 270
          },
          {
            label: 'From',
            field: 'fromDate',
            width: 100,
            attributes: {className: 'text-center'}
          },
          {
            label: 'To',
            field: 'toDate',
            width: 100,
            attributes: {className: 'text-center'}
          },
          {
            label: 'Approve',
            field: 'approveLeave',
            width: 100,
            attributes: {className: 'text-center'}
          },
          {
            label: 'Reject',
            field: 'rejectLeave',
            width: 100,
            attributes: {className: 'text-center'}
          }
        ],
        rows: null
    };

    return (
        <div className="container mt-5">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <h4 className="main-header-style"><strong>Broadweb Digital HR Management System</strong></h4>
                    <div className="card">
                        <div className="card-header">
                            <strong>Dashboard</strong>
                            <PowerOff className="svg-logout-style" onClick={logOut}/>
                            <span className='text-align-right role-style'><strong><small>(Role: {localStorage.getItem('roleName')})</small></strong></span> 
                        </div>
                        <div className="card-body">
                            Welcome : {userEmail}
                            <span className='rounded custom-auths' onClick={()=>{history.push('/roles')}}>Manage Permissions <ManagePermissions/></span>
                            <span className='mx-2 rounded custom-auths' onClick={()=>{history.push('/roles')}}>Manage Roles <ManageRoles/></span> 
                        </div>
                    </div>
                    <div className="row row-cols-1 row-cols-md-4 mt-3">
                        <div className="col mb-4" onClick={()=>{history.push("/employees")}} style={{cursor: "pointer"}}>
                            <div className="card">
                            <div className="card-body">
                                <h6 className="card-title"><strong>Total Employees</strong></h6>
                                <TotalEmployees className='card-icon-style' /> <span className='text-align-right card-text-style'>5</span>
                            </div>
                            </div>
                        </div>
                        <div className="col mb-4" style={{cursor: "pointer"}}>
                            <div className="card">
                            <div className="card-body">
                                <h6 className="card-title"><strong>Casual Leaves</strong></h6>
                                <CasualLeaves className='card-icon-style' /> <span className='text-align-right card-text-style'>10</span>
                            </div>
                            </div>
                        </div>
                        <div className="col mb-4" style={{cursor: "pointer"}}>
                            <div className="card">
                            <div className="card-body">
                                <h6 className="card-title"><strong>Sick Leaves</strong></h6>
                                <SickLeave className='card-icon-style' /> <span className='text-align-right card-text-style'>3</span>
                            </div>
                            </div>
                        </div>
                        <div className="col mb-4" style={{cursor: "pointer"}}>
                            <div className="card">
                            <div className="card-body">
                                <h6 className="card-title"><strong>All leaves</strong></h6>
                                <TotalLeaves className='card-icon-style' /> <span className='text-align-right card-text-style'>13</span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header">
                            <strong>Leave Requests</strong>
                        </div>
                        <div className="card-body">
                            <MDBDataTable 
                                striped 
                                bordered 
                                small
                                entries={5}
                                data = {data}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
